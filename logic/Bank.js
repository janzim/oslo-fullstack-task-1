/**
 * Bank that holds balance and loan info
 */
export const Bank = {
    balance: 0,
    hasLoan: false,
    getLoan: function(loanInput) {    
        if(loanInput <= this.balance * 2 && loanInput > 0 && !this.hasLoan){
            this.balance += loanInput;
            this.hasLoan = true;
            return true;
        } else {
            return false;
        }
    }
}