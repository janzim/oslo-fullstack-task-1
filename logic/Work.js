import { Bank } from "./Bank.js"

/**
 * Work that holds pay and possibility to work
 */
export const Work = {
    pay: 0,
    transferToBank: function() { 
        Bank.balance += this.pay;
        this.pay = 0;
    },
    work: function() {
        this.pay += 100;
    }
}