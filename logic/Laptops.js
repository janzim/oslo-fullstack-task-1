/**
* An array of laptops with relevant info
*/
export const Laptops = [
   {
       name: "Intel Cerion i19",
       price: 5000,
       description: "With its fully new i19 128 cores 256 threads processor, it can run even the oldest of games!",
       features: "Will break if you drop it!",
       image: "https://www.komplett.no/img/p/1200/1155859.jpg"
   },
   {
       name: "AMD Alien Explosion",
       price: 1500,
       description: "A computer that satisfies all your browsing needs",
       features: "(Will not cause an explosion)",
       image: "https://i.dell.com/sites/csimages/Video_Imagery/all/aw-m15-hd-rollupimage.png"
   },
   {
       name: "Dell Ph1829",
       price: 15000,
       description: "Beefy and ready to be pushed to the maximum to deliver you the best gaming experience",
       features: "Will help you get those 5 extra FPS",
       image: "https://5.imimg.com/data5/RH/KJ/FH/SELLER-19266809/2-1-500x500.jpg"
   },
   {
       name: 'Asus Phantom Attack ("Cars" version)',
       price: 250,
       description: "Supports text typing and calculations for your everyday needs. Bag included.",
       features: "KA-CHOWW!",
       image: "https://thumbs.worthpoint.com/zoom/images1/1/1211/08/vtech-lightning-mcqueen-bilingual_1_ff7a1ec09c2fab9cca4a361238802c8f.jpg"
   }
]