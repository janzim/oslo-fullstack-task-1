import { Bank } from "./logic/Bank.js"
import { Work } from "./logic/Work.js"
import { Laptops } from "./logic/Laptops.js"

/**
 * Logic function to buy a laptop
 * @param {The laptop object to be bought} laptopObj
 * @returns true/false if laptop has been purchased or not
 */
const buyLaptop = laptopObj => {
    if(Bank.balance >= laptopObj.price){
        Bank.hasLoan = false;
        Bank.balance -= laptopObj.price;
        return true;
    } else {
        return false;
    }
}

/**
 * GUI
 */
const elBalance = document.getElementById('balance');
const elPay = document.getElementById('pay');
const elLoanBtn = document.getElementById('loanBtn');
const elBankBtn = document.getElementById('bankBtn');
const elWorkBtn = document.getElementById('workBtn');
const elBuyBtn = document.getElementById('buyBtn');
const elLaptopImg = document.getElementById('laptopImg');
const elLaptops = document.getElementById('laptops');
const elFeatures = document.getElementById('features');
const elDescription = document.getElementById('description');
const elPrice = document.getElementById('price');
const elHistory = document.getElementById('history');

/**
 * Add a new laptop to the "history" list
 */
const addNewHistoryItem = (laptop) => {
    let elHistoryDiv = document.createElement('div');
    elHistoryDiv.className = 'history-item';
    let elHistoryImage = document.createElement('img');
    elHistoryImage.className = 'laptopImgSmall';
    elHistoryImage.src = laptop.image;
    let elHistoryText = document.createElement('h6');
    elHistoryText.style = 'float: top';
    elHistoryText.innerText = `"${laptop.name}" for ${laptop.price} kr`;
    elHistoryDiv.appendChild(elHistoryImage);
    elHistoryDiv.appendChild(elHistoryText);
    elHistory.appendChild(elHistoryDiv);
}

elLoanBtn.onclick = () => {
    if(Bank.hasLoan){
        alert("You have already taken a loan!");
        return;
    }

    let loanInput = prompt(`How much do you want to loan? Max is ${Bank.balance*2} kr`);
    if(Bank.getLoan(parseInt(loanInput))){
        elBalance.innerText = Bank.balance + ' kr';
        alert(`You have successfully lend ${loanInput} kr`);
    } else if(loanInput != null) {
        alert(`You're trying to lend too much! Max is ${Bank.balance*2} kr`);
    }
}

elBankBtn.onclick = () => {
    Work.transferToBank();
    elBalance.innerText = Bank.balance + ' kr';
    elPay.innerText = Work.pay + ' kr';
}

elWorkBtn.onclick = () => {
    Work.work();
    elPay.innerText = Work.pay + ' kr';
}

elBuyBtn.onclick = () => {
    const currentLaptop = Laptops[elLaptops.value];
    if(buyLaptop(currentLaptop)){
        elBalance.innerText = Bank.balance + ' kr';
        
        // add new item to history list
        addNewHistoryItem(currentLaptop);

        // throw alert to user
        alert(`You have successfully bought ${currentLaptop.name} for ${currentLaptop.price} kr`);
    } else {
        alert(`Insufficient funds (${Bank.balance} kr) to buy laptop (${currentLaptop.price} kr)`)
    }
}

/**
 * Change image, feature, price, etc. whenever user wants
 * to see another laptop
 */
const doLaptopChange = () => {
    const currentLaptop = Laptops[elLaptops.value];
    elLaptopImg.src = currentLaptop.image;
    elFeatures.innerText = currentLaptop.features;
    elDescription.innerText = currentLaptop.description;
    elPrice.innerText = currentLaptop.price + " kr";
}

// load all laptop options into the <select>
var opt = null;
for(let i = 0; i < Laptops.length; i++){
    opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = Laptops[i].name;
    elLaptops.appendChild(opt);
}

// set default values on first open
doLaptopChange();

elLaptops.onchange = () => doLaptopChange();