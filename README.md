# Oslo Java-Fullstack Task 1
Javascript website that lets the user "buy" a computer. The user can work and bank the money, and also take a loan to buy the computer for the given price. When a computer has been bought, it will show up on a history list.

# Showcase 
![](showcase/showcase2.PNG)

----------------------------------------------

![](showcase/showcase3.PNG)

# Author
Jan Zimmer